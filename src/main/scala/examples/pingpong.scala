package examples

import akka.actor.{Actor, Props}
import akka.routing.RoundRobinPool
import com.typesafe.scalalogging.LazyLogging
import examples.PingPongProtocol.{Ping, Pong, Start}

object PingActor {
  def props = Props[PingActor]
}

object PingPongProtocol {
  case object Start
  case class Ping(who : String)
  case class Pong(who : String, count : Int)
}

class PingActor extends Actor with LazyLogging {
  private val maxPing = 100
  private val pongActor = context.actorOf(props = PongActor.props(maxPing), name = "pongActor")

  override def preStart(): Unit = logger.info(s"${self.path} - I'm alive!")

  override def postStop(): Unit = logger.info(s"${self.path} - I'm dead. :(")

  override def receive: Receive = {
    case Start =>
      logger.info("Starting PingActor!")
      pongActor ! Ping("Some_0")

    case Pong(who, count) =>
      logger.info(s"Ponged for $who - total: $count pongs!")
      if (count < maxPing) {
        sender ! Ping(s"Some_$count")
      } else {
        context.stop(self)
      }

    case unexpected => logger.warn(s"Hmmm, we got something odd... $unexpected")
  }
}

object PongActor {
  def props(maxPing : Int) = Props(new PongActor(maxPing))
}

class PongActor(maxPing : Int) extends Actor with LazyLogging {
  var pingCount = 0

  override def preStart(): Unit = logger.info(s"${self.path} - I'm alive!")

  override def postStop(): Unit = logger.info(s"${self.path} - Darn, I only processed $pingCount pings!")

  override def receive: Receive = {
    case Ping(who) =>
      logger.info(s"Ping for $who...")
      pingCount = pingCount + 1
      sender ! Pong(who, pingCount)
  }
}