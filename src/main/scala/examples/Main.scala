package examples

import akka.actor.ActorSystem
import examples.PingPongProtocol.Start

object Main extends App {
  val system = ActorSystem("PingSystem")
  val pingActor = system.actorOf(props = PingActor.props, name = "pingActor")
  pingActor ! Start
}
