package examples;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

public class JMain {
  public static void main(String[] args) {
      ActorSystem system = ActorSystem.create("JavaPingSystem");
      ActorRef pingActor = system.actorOf(Props.create(JPingActor.class), "pingActor");
      pingActor.tell(new JPingActor.Start(), null);

  }
}
