package examples;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;

public class JPingActor extends AbstractLoggingActor {
    // protocol
    public static class Ping {
        private final String who;

        public Ping(String who) {
            this.who = who;
        }

        public String getWho() {
            return who;
        }
    }

    public static class Start {}

    public static class Pong {
        private final String who;
        private final Integer count;

        public Pong(String who, Integer count) {
            this.who = who;
            this.count = count;
        }

        public String getWho() {
            return who;
        }

        public Integer getCount() {
            return count;
        }
    }

    private final Integer maxPing = 10;
    private final ActorRef pongActor = getContext().actorOf(Props.create(JPongActor.class), "pongActor");

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(
                    Start.class,
                    s -> {
                        log().info("Starting PingActor!");
                        pongActor.tell(new Ping("Some_0"), getSelf());
                    }
                )
                .match(
                        Pong.class,
                        p -> {
                            log().info("Received ping from {} - total count is {}", p.getWho(), p.getCount());
                            if (p.getCount() < maxPing) {
                                sender().tell(new Ping("Some_" + p.getCount()), getSelf());
                            } else {
                                getContext().stop(getSelf());
                            }
                        }
                ).build();
    }

    @Override
    public void preStart() throws Exception {
        log().info("{} starting up!", getSelf().path());
    }

    @Override
    public void postStop() throws Exception {
        log().info("{} stopping!", getSelf().path());
    }
}