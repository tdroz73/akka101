package examples;

import akka.actor.AbstractLoggingActor;

public class JPongActor extends AbstractLoggingActor {
    private Integer count = 0;

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(
                        JPingActor.Ping.class,
                        p -> {
                            count = count + 1;
                            log().info("Received ping from {}", p.getWho());
                            sender().tell(new JPingActor.Pong(p.getWho(), count), getSelf());
                        }
                ).build();
    }

    @Override
    public void preStart() throws Exception {
        log().info("{} starting up!", getSelf().path());
    }

    @Override
    public void postStop() throws Exception {
        log().info("{} stopping!", getSelf().path());
    }

}